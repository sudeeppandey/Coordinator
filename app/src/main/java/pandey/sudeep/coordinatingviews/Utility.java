package pandey.sudeep.coordinatingviews;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class Utility {

    public static byte[] getByteArray(Object o){

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream( baos );
        } catch (IOException e) {
            //e.printStackTrace();
        }
        try {
            oos.writeObject( o );
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return baos.toByteArray();
    }

    public static Object getBackObject(byte[] byteArray){

        ByteArrayInputStream bais = new ByteArrayInputStream( byteArray );
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream( bais );
        } catch (IOException e) {
            //e.printStackTrace();
        }
        Object o = null;
        try {
            o = ois.readObject();
        } catch (IOException e) {
            //e.printStackTrace();
        } catch (ClassNotFoundException e) {
            //e.printStackTrace();
        }
        return o;
    }
}
