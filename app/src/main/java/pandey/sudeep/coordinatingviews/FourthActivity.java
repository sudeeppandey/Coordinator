package pandey.sudeep.coordinatingviews;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class FourthActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    //private LinearLayoutManager linearLayoutManager;
    private TheAdapter adapter;
    private List<MyCarrier> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fourth);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("4");
        toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorTextMain));
        setSupportActionBar(toolbar);

        data = new ArrayList<MyCarrier>();

        data.add(0, new MyCarrier("Barcelona", R.drawable.barcelona));
        data.add(1, new MyCarrier("Cordoba", R.drawable.cordoba));
        data.add(2, new MyCarrier("Gran Canaria", R.drawable.gran_canaria));
        data.add(3, new MyCarrier("Granada", R.drawable.granada));
        data.add(4, new MyCarrier("Ibiza", R.drawable.ibiza));
        data.add(5, new MyCarrier("Lanzarote", R.drawable.lanzarote));
        data.add(6, new MyCarrier("Madrid", R.drawable.madrid));
        data.add(7, new MyCarrier("Balearic", R.drawable.balearic));
        data.add(8, new MyCarrier("Majorca", R.drawable.majorca));
        data.add(9, new MyCarrier("Malaga", R.drawable.malaga));
        data.add(10, new MyCarrier("Palma", R.drawable.palma));
        data.add(11, new MyCarrier("Sevilla", R.drawable.sevilla));
        data.add(12, new MyCarrier("Teida", R.drawable.teide));
        data.add(13, new MyCarrier("Tenerife", R.drawable.tenerife));
        data.add(14, new MyCarrier("Valencia", R.drawable.valencia));

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        //mLayoutManager = new GridLayoutManager(this, 2);
        //mLayoutManager = new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);

        adapter = new TheAdapter(data);
        recyclerView.setAdapter(adapter);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setRippleColor(getResources().getColor(R.color.cardview_light_background));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), FifthActivity.class));

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.remove:
                // implementation
                return true;

            case R.id.settings:
                //implementation
                return true;

            case R.id.touch:
                //implementation
                return true;

            default:
                // implementation
                return super.onOptionsItemSelected(item);
        }
    }
}
