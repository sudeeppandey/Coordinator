package pandey.sudeep.coordinatingviews;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class TheAdapter extends RecyclerView.Adapter<TheAdapter.ViewHolder> {

    private List<MyCarrier> dataCarrier;
    //private Listener listener;

    //interface Listener {
      //  void onClick(int position);
    //}

    public  static class ViewHolder extends RecyclerView.ViewHolder{

        public CardView cardView;

        public ViewHolder(CardView cv){
            super(cv);
            this.cardView=cv;
        }
    }

    public TheAdapter(List<MyCarrier> toBeSupplied){
        this.dataCarrier = toBeSupplied;
    }

    @Override
    public int getItemCount(){
        return dataCarrier.size();
    }
    //public void setListener(Listener listener){
      //  this.listener = listener;
    //}

    @Override
    public TheAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {

        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mycard, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position){

        final CardView cardView = holder.cardView;
        ImageView imageView = (ImageView)cardView.findViewById(R.id.imageView);
        final TextView textView = (TextView)cardView.findViewById(R.id.textView);
        imageView.setImageResource(dataCarrier.get(position).getImageResourceID());
        textView.setText(dataCarrier.get(position).getName());
       // cardView.setOnClickListener(new View.OnClickListener() {
         //   @Override
           // public void onClick(View v) {
             //   textView.setText("clicked");
               // if(listener != null){
                    //listener.onClick(position);
                //}
            //}
        //});
    }
}
