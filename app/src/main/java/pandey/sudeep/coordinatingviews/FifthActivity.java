package pandey.sudeep.coordinatingviews;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class FifthActivity extends AppCompatActivity {

    private ViewPager pager;

    private List<MyCarrier> spanish = new ArrayList<MyCarrier>();
    private List<MyCarrier> swiss = new ArrayList<MyCarrier>();
    private List<MyCarrier> norweign = new ArrayList<MyCarrier>();

    CollapsingToolbarLayout collapsingToolbarLayout;
    Context context = this;

    static TheAdapter spainAdapter = null;
    static TheAdapter swissAdapter = null;
    static TheAdapter norweignAdapter = null;

    DemoCollectionPagerAdapter mDemoCollectionPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fifth);

        spanish.add(0, new MyCarrier("Barcelona", R.drawable.barcelona));
        spanish.add(1, new MyCarrier("Cordoba", R.drawable.cordoba));
        spanish.add(2, new MyCarrier("Gran Canaria", R.drawable.gran_canaria));
        spanish.add(3, new MyCarrier("Granada", R.drawable.granada));
        spanish.add(4, new MyCarrier("Ibiza", R.drawable.ibiza));
        swiss.add(0, new MyCarrier("Zurich", R.drawable.zurich));
        swiss.add(1, new MyCarrier("St.Gallen", R.drawable.stgallen));
        swiss.add(2, new MyCarrier("Berne", R.drawable.berne));
        swiss.add(3, new MyCarrier("Weesen", R.drawable.weesen));
        swiss.add(4, new MyCarrier("ZermatValley", R.drawable.zermattvalley));
        norweign.add(0, new MyCarrier("Trondheim", R.drawable.trondheim));
        norweign.add(1, new MyCarrier("Bergen", R.drawable.bergen));
        norweign.add(2, new MyCarrier("Oslo", R.drawable.oslo));
        norweign.add(3, new MyCarrier("Nordregio", R.drawable.nordregio));
        norweign.add(4, new MyCarrier("Storgata", R.drawable.storgata));

        spainAdapter = new TheAdapter(spanish);
        swissAdapter = new TheAdapter(swiss);
        norweignAdapter = new TheAdapter(norweign);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.drawable.ic_lightbulb_outline_black_24dp);
        setSupportActionBar(toolbar);

        collapsingToolbarLayout = findViewById(R.id.collapse);
        collapsingToolbarLayout.setTitle("Collapse");
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.holo_blue_bright));
        collapsingToolbarLayout.setCollapsedTitleTextColor((getResources().getColor(R.color.colorTextMain)));

        pager = findViewById(R.id.pager);
        final TabLayout tabLayout = (TabLayout)findViewById(R.id.tabs);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mDemoCollectionPagerAdapter = new DemoCollectionPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(mDemoCollectionPagerAdapter);
        tabLayout.setupWithViewPager(pager);


        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setRippleColor(getResources().getColor(R.color.cardview_light_background));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, SixthActivity.class));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public class DemoCollectionPagerAdapter extends FragmentStatePagerAdapter {

        public DemoCollectionPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {

            if(i==0){

                Fragment fragment = new SpainFragment();
                return fragment;
            }
            if(i==1){

                Fragment fragment = new SwissFragment();
                return fragment;
            }
            if(i==2){
                Fragment fragment = new NorweignFragment();
                return fragment;
            }

            return null;

        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {

            if(position==0){

                return "SPAIN";
            }

            if(position==1){
                return "SWITZERLAND";
            }
            if(position==2){
                return "NORWAY";
            }

            return null;
        }
    }


    public static class SpainFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(
                    R.layout.fragment, container, false);
            RecyclerView recyclerView = rootView.findViewById(R.id.rView);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(spainAdapter);
            return rootView;
        }
    }

    public static class SwissFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(
                    R.layout.fragment, container, false);
            RecyclerView recyclerView = rootView.findViewById(R.id.rView);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(swissAdapter);
            return rootView;
        }
    }

    public static class NorweignFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {

            View rootView = inflater.inflate(
                    R.layout.fragment, container, false);
            RecyclerView recyclerView = rootView.findViewById(R.id.rView);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(norweignAdapter);
            return rootView;
        }
    }
}
